/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const unsigned int gappih    = 7;        /* horiz inner gap between windows */
static const unsigned int gappiv    = 7;        /* vert inner gap between windows */
static const unsigned int gappoh    = 7;        /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 7;        /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int horizpadbar        = 20;        /* horizontal padding for statusbar */
static const double activeopacity   = 1.0f;     /* Window opacity when it's focused (0 <= opacity <= 1) */
static const double inactiveopacity = 0.875f;   /* Window opacity when it's inactive (0 <= opacity <= 1) */
static       Bool bUseOpacity       = True;     /* Starts with opacity on any unfocused windows */
static const char *fonts[]          = { "JetBrainsMono Nerd Font:style=ExtraBold,Regular:size=9" };
static const char dmenufont[]       = "JetBrainsMono Nerd Font:style=ExtraBold,Regular:size=9";
#define ICONSIZE (bh - 2)           /* icon size */
#define ICONSPACING (bh - 10)       /* space between icon and title */
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#ec5f67";
static char selbgcolor[]            = "#ec5f67";
static char titlebgcolor[]          = "#bbbbbb";
static char termcol0[] = "#343d46"; /* black   */
static char termcol1[] = "#ec5f67"; /* red     */
static char termcol2[] = "#99c794"; /* green   */
static char termcol3[] = "#fac863"; /* yellow  */
static char termcol4[] = "#6699cc"; /* blue    */
static char termcol5[] = "#e27e8d"; /* magenta */
static char termcol6[] = "#5fb3b3"; /* cyan    */
static char termcol7[] = "#d8dee9"; /* white   */
static char termcol8[]  = "#6f777f"; /* black   */
static char termcol9[]  = "#ed7777"; /* red     */
static char termcol10[] = "#a9d4a9"; /* green   */
static char termcol11[] = "#fad17f"; /* yellow  */
static char termcol12[] = "#77add9"; /* blue    */
static char termcol13[] = "#e298a3"; /* magenta */
static char termcol14[] = "#6bbfbf"; /* cyan    */
static char termcol15[] = "#ebf5f5"; /* white   */
static char *termcolor[] = {
  termcol0, termcol1, termcol2, termcol3, termcol4, termcol5, termcol6, termcol7,
  termcol8, termcol9, termcol10, termcol11, termcol12, termcol13, termcol14, termcol15,
};
static char *colors[][3] = {
       /*               fg           bg            border   */
       [SchemeNorm] = { normfgcolor, normbgcolor,  normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,   selbordercolor  },
       [SchemeInac] = { termcol8,    normbgcolor,  normbordercolor },
       [SchemeTitle]= { normfgcolor, titlebgcolor, normbordercolor },
};

static const char *const autostart[] = {
	"lxsession", NULL,
	"sh", "-c", "~/.config/scripts/CacheLockScreen.sh", NULL,
	"/usr/bin/gnome-keyring-daemon", "--start", "--components=pkcs11,secrets,ssh", NULL,
	"kded5", NULL,
	"picom", NULL,
	"nitrogen", "--restore", NULL,
	"/usr/bin/dunst", NULL,
	"nm-applet", NULL,
	"udiskie", "--tray", NULL,
	"/usr/bin/sxhkd", NULL,
	// "sh", "-c", "~/.Scripts/HuionConfig.sh", NULL,
	// "sh", "-c", "~/.config/polybar/launch.sh", NULL,
	// "", NULL,
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "" };

/* default layout per tags */
/* The first element is for all-tag view, following i-th element corresponds to */
/* tags[i]. Layout is referred using the layouts array index.*/
static int def_layouts[1 + LENGTH(tags)]  = { 0, 2, 0, 1, 1, 3, 3 };


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class             instance       title       tags mask     isfloating   monitor */
	/* 1) Terminal */
	//{ "Alacritty",       NULL,          NULL,        1,               0,         -1 },
	//{ "kitty",           NULL,          NULL,        1,               0,         -1 },

	/* 2) Code */
	{ "code-oss",        NULL,          NULL,        1 << 1,          0,         -1, 0 },
	{ "Emacs",           NULL,          NULL,        1 << 1,          0,         -1, 0 },
	{ "jetbrains-clion", NULL,          NULL,        1 << 1,          0,         -1, 1 },
	{ "jetbrains-rider", NULL,          NULL,        1 << 1,          0,         -1, 1 },
	{ "UE4Editor",       NULL,          NULL,        1 << 1,          0,         -1, 1 },

	/* 3) WWW */
	{ "Chromium",        NULL,          NULL,        1 << 2,          0,         -1, 0 },
	{ "Brave-browser",   NULL,          NULL,        1 << 2,          0,         -1, 0 },
	{ "firefox",         NULL,          NULL,        1 << 2,          0,         -1, 0 },
	{ "Google-chrome",   NULL,          NULL,        1 << 2,          0,         -1, 0 },
	{ "Vivaldi-stable",  NULL,          NULL,        1 << 2,          0,         -1, 0 },

	/* 4) GFX */
	{ "Blender",         NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "Gimp",            NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "krita",           NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "Steam",           NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "TexturePacker",   NULL,          NULL,        1 << 3,          0,         -1, 0 },

	/* 5) Music */
	{ "Cadence",         NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "elisa",           NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "Guitarix",        NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "Spotify",         NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "Stremio",         NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "cantata",         NULL,          NULL,        1 << 4,          0,         -1, 0 },

	/* 6) Chat */
	{ "discord",         NULL,          NULL,        1 << 5,          0,         -1, 0 },
	{ NULL,              NULL,  "FacebookMessenger", 1 << 5,          0,         -1, 0 },
	{ "Slack",           NULL,          NULL,        1 << 5,          0,         -1, 0 },
	{ NULL,              NULL,       "WhatsApp",     1 << 5,          0,         -1, 0 },
};

/* layout(s) */
static const float mfact     = 0.65; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	// { "[@]",      spiral },
	// { "[\\]",     dwindle },
	// { "H[]",      deck },
	// { "TTT",      bstack },
	// { "===",      bstackhoriz },
	// { "HHH",      grid },
	// { "###",      nrowgrid },
	// { "---",      horizgrid },
	// { ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	// { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	// { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	// { MODKEY,                       XK_b,      togglebar,      {0} },
	// { MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	// { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	// { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	// { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	// { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	// { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	// { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	// { MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	// { MODKEY,                       XK_Return, zoom,           {0} },
	// { MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	// { MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
	// { MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	// { MODKEY,                       XK_Tab,    view,           {0} },
	// { MODKEY,                       XK_a,      toggleopacity,  {0} },
	// { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	// { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	// { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	// { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	// { MODKEY,                       XK_space,  setlayout,      {0} },
	// { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	// { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	// { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	// { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	// { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	// { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	// TAGKEYS(                        XK_1,                      0)
	// TAGKEYS(                        XK_2,                      1)
	// TAGKEYS(                        XK_3,                      2)
	// TAGKEYS(                        XK_4,                      3)
	// TAGKEYS(                        XK_5,                      4)
	// TAGKEYS(                        XK_6,                      5)
	// TAGKEYS(                        XK_7,                      6)
	// TAGKEYS(                        XK_8,                      7)
	// TAGKEYS(                        XK_9,                      8)
	{ MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },
	{ MODKEY|ShiftMask,             XK_y,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
	{ "quit",           quit },

	{ "killclient",     killclient },
	{ "focusstack",     focusstack },
	{ "movestack",      movestack },
	{ "rotatestack",    rotatestack },
	{ "zoom",           zoom },
	{ "focusmaster",    focusmaster },
	{ "incnmaster",     incnmaster },

	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
	{ "setmfact",       setmfact },
//	{ "setcfact",       setcfact },
	{ "focusmon",       focusmon },
	{ "tagmon",         tagmon },

	{ "view",           view },
//	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
//	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "togglebar",      togglebar },
	{ "toggleopacity",  toggleopacity },
	{ "togglefloating", togglefloating },

};
